﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] string playerTag;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == playerTag)
        {
            collision.GetComponent<PlayerController>().DealDamage();
            Destroy(gameObject);
        }
    }
}
