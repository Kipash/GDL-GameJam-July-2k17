﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossFightController : MonoBehaviour
{
    [SerializeField] GameObject[] torches;
    [SerializeField] float minDistance;
    [SerializeField] float delay;
    [SerializeField] float startDelay;
    Stack<GameObject> torchesStack;
    
    [Header("Torches settings")]
    [SerializeField] Color normal;
    [SerializeField] Sprite extinguished;

    GameObject currTorch;
    float currDistance;

    bool inInteractiveRange;
    void Start()
    {
        torchesStack = new Stack<GameObject>(torches);
        Invoke("PickATorch", startDelay);
    }

    void Update()
    {
        if(currTorch != null)
        {
            currDistance = Vector3.Distance(transform.position, currTorch.transform.position);
            if(currDistance < minDistance)
            {
                inInteractiveRange = true;
            }
            else
            {
                inInteractiveRange = false;
            }

        }
        else
        {
            inInteractiveRange = false; 
        }
           

        if(inInteractiveRange)
        {
            //UI
            if(Input.GetKeyDown(KeyCode.E))
            {
                ExtinguishTorch();
            }
        }

    }
    void PickATorch()
    {
        currTorch = torchesStack.Pop();
        var sr = currTorch.GetComponent<SpriteRenderer>();
        sr.color = normal;
    }

    void ExtinguishTorch()
    {
        currTorch.GetComponent<Animator>().enabled = false;
        var sr = currTorch.GetComponent<SpriteRenderer>();


        sr.sprite = extinguished;
        currTorch = null;
        if (torchesStack.Count == 0)
        {
            Application.LoadLevel(Application.loadedLevel + 1);
            return;
        }

        Invoke("PickATorch", delay);
    }
}
