﻿using UnityEngine;
using System.Collections;

public class BossEnemyController : MonoBehaviour
{
    [Header("Points")]
    [SerializeField] Transform rockShootSpot;

    [Header("Stats")]
    [SerializeField] float attackRate;
    [SerializeField] float speedOfShots;

    [Header("Rock")]
    [SerializeField] GameObject rock;

    [Header("Effects")]
    [SerializeField] Animator animator;
    [SerializeField] SpriteRenderer spriteRenderer;
    [SerializeField] string idleTrigger;
    [SerializeField] string attackTrigger;
    [SerializeField] string dieTrigger;
    [SerializeField] bool flipOrientation;

    [Header("Player")]
    [SerializeField] string playerTag;

    GameObject player;
    
    

    void Start()
    {
        player = GameObject.FindGameObjectWithTag(playerTag);
        InvokeRepeating("Attack", 2, 2);
    }

    void GetHit()
    {

    }

    void Attack()
    {
        ThrowRock();
    }

    void ThrowRock()
    {
        var go = Instantiate(rock, rockShootSpot.position, rockShootSpot.rotation);
        var rb = go.GetComponent<Rigidbody2D>();
        rb.AddForce(rockShootSpot.forward * speedOfShots);
        ///
        Destroy(go, 10);
    }

    void Update()
    {
        rockShootSpot.LookAt(player.transform);
        

    }

    void FlipSprite()
    {
        //spriteRenderer.flipX = ;
    }
}
