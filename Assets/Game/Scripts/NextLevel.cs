﻿using UnityEngine;
using System.Collections;

public class NextLevel : MonoBehaviour
{
    public void MoveToNextLevel()
    {
        Application.LoadLevel(Application.loadedLevel + 1);
    }
}
