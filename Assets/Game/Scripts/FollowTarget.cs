﻿using UnityEngine;
using System.Collections;

public class FollowTarget : MonoBehaviour
{
    [SerializeField] Transform target;
    [SerializeField] float offsetX;
    [SerializeField] float offsetY;
    [SerializeField] float speedX;
    [SerializeField] float speedY;
    
    void FixedUpdate()
    {
        transform.position =
            new Vector3(
                Mathf.Lerp(
                    transform.position.x
                    , offsetX + target.position.x
                    , 0.5f)
                , Mathf.Lerp(
                    transform.position.y
                    , offsetY + target.position.y
                    , 0.5f)
                , transform.position.z);        
    }
}