﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

[System.Serializable]
public class Line
{
    public Transform P1;
    public Transform P2;
}

[System.Serializable]
public class Mode
{
    public float Speed;
    public float LinearDrag;
    public float Gravity;

    public Color Color;
    public Sprite Sprite;
}

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerController : MonoBehaviour {

    [Header("Player stats")]
    [SerializeField] float jumpForce;

    [Header("Anims")]
    [SerializeField] Animator anim;

    [Header("ClassicMode")]
    [SerializeField] Mode classicMode;
    [Header("AirMode")]
    [SerializeField] Mode airMode;

    [Header("GroundCheck")]
    [SerializeField] Line[] groundCheckPoints;
    [SerializeField] LayerMask mask;
    

    Rigidbody2D rigidbody;
    [SerializeField] SpriteRenderer spriteRenderer;
    
    Mode currMode;
    int HP;
    
    public Mode CurrMode
    {
        get
        {
            return currMode;
        }
        set
        {
            currMode = value;
            spriteRenderer.sprite = value.Sprite;
            spriteRenderer.color = value.Color;

            rigidbody.drag = value.LinearDrag;
            rigidbody.gravityScale = value.Gravity;
        }
    }
    Mode beforeAir;

    float jumpTrashhold;

    bool isGrounded;
    bool isGroundedLastFrame;
    bool isFlippedX;
    bool isFlippedXLastFrame;


    void Start () {
        HP = 3;
        rigidbody = GetComponent<Rigidbody2D>();

        CurrMode = airMode;
	}

    float forceX;
    float forceY;

    void Update ()
    {
        isGrounded = GroundCheck();

        if (Input.GetKeyDown(KeyCode.Space) && isGrounded && Time.time > jumpTrashhold)
        {
            print("====");
            print("jump");
            jumpTrashhold = Time.time + 0.2f;



            rigidbody.velocity = Vector2.zero;
            rigidbody.angularVelocity = 0;

            var force = jumpForce * 100;
            print("f: " + force);

            rigidbody.AddForce(Vector2.up * force, ForceMode2D.Force);
            print("====");

        }


        if (Input.GetKeyDown(KeyCode.F1))
        {
            FindObjectsOfType<PerlinShake>()[0].testPosition = true;
        }

        if(isGrounded != isGroundedLastFrame)
        {
            
            if(isGrounded)
            {
                CurrMode = classicMode;
                anim.SetTrigger("Land");
            }
            else
            {
                CurrMode = airMode;
                anim.SetTrigger("Jump");
            }
        }

        if(isFlippedX != isFlippedXLastFrame)
        {
            if (isFlippedX)
                spriteRenderer.flipX = true;
            else
                spriteRenderer.flipX = false;
        }

        foreach (var x in groundCheckPoints)
            Debug.DrawLine(x.P1.position, x.P2.position, Color.magenta);

        isGroundedLastFrame = isGrounded;
        isFlippedXLastFrame = isFlippedX;
    }
    void FixedUpdate()
    {

        if (Input.GetKey(KeyCode.A))
        {
            rigidbody.AddForce(Vector2.left * CurrMode.Speed * 100 * Time.deltaTime, ForceMode2D.Force);
            isFlippedX = false;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            rigidbody.AddForce(Vector2.right * CurrMode.Speed * 100 * Time.deltaTime, ForceMode2D.Force);
            isFlippedX = true;
        }


    }

    bool GroundCheck()
    {
        foreach (var p in groundCheckPoints)
        {
            if (Physics2D.Linecast(p.P1.position, p.P2.position, mask))
                return true;
        }
        return false;
    }
    public void DealDamage()
    {
        HP--;

        Camera.main.GetComponent<ColorCorrectionCurves>().saturation -= 0.34f;
        Camera.main.GetComponent<PerlinShake>().testPosition = true;

        if (HP == 0)
            Die();
    }
    void Die()
    {
        Camera.main.GetComponent<Animator>().SetTrigger("Die");
        Invoke("ResetGame", 3);
    }
    void ResetGame()
    {
        Application.LoadLevel(Application.loadedLevel);
    }

    void OnGUI()
    {
        GUI.Label(new Rect(10, Screen.height / 4, 250, 20), "isGrounded: " + (isGrounded ? "grounded" : "in air"));
    }
}
