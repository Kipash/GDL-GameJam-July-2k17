﻿using UnityEngine;
using System.Collections;

public class JumpPadController : MonoBehaviour
{
    [SerializeField] float jumpPower;
    [SerializeField] string tag;
    [SerializeField] Vector2 direction;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == tag)
        {
            var rb = collision.GetComponent<Rigidbody2D>();
            rb.isKinematic = true;
            rb.isKinematic = false;
            rb.velocity = Vector2.zero;
            rb.angularVelocity = 0;
            rb.AddForce(direction * jumpPower * 100 * Time.fixedDeltaTime, ForceMode2D.Force);
        }
    }
}
