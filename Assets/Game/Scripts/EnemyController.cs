﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour
{
    [Header("Points")]
    [SerializeField] Transform patrolStart;
    [SerializeField] Transform patrolEnd;

    [Header("Stats")]
    [SerializeField] float speed;
    [SerializeField] float waitTrashhold;
    [SerializeField] float minDistanceToPatrolPoint;
    [SerializeField] float minXDistanceToAttack;
    [SerializeField] float minYDistanceToAttack;
    [SerializeField] float attackRate;

    [Header("Effects")]
    [SerializeField] Animator animator;
    [SerializeField] SpriteRenderer spriteRenderer;
    [SerializeField] string idleTrigger;
    [SerializeField] string walkTrigger;
    [SerializeField] string attackTrigger;
    [SerializeField] string dieTrigger;
    [SerializeField] bool flipOrientation;

    [Header("Player")]
    [SerializeField] string playerTag;

    GameObject player;
    float playerDistanceX;
    float playerDistanceY;
    Vector3 targetPos;
    bool patrol;
    bool isWaiting;
    bool isAtacking;
    bool isAtackingLastFrame;


    float attackTrashhold;
    float velocity;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag(playerTag);
        targetPos = patrolStart.position;
        ResetPatrol();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == playerTag)
        {
            animator.ResetTrigger(idleTrigger);
            animator.ResetTrigger(walkTrigger);
            animator.ResetTrigger(attackTrigger);
            animator.SetTrigger(dieTrigger);

            Destroy(this);
        }
    }

    void Update()
    {
        var distance = transform.position - player.transform.position;

        playerDistanceX = Mathf.Abs(distance.x);
        playerDistanceY = Mathf.Abs(distance.y);

        if (playerDistanceX < minXDistanceToAttack && playerDistanceY < minYDistanceToAttack)
        {
            isAtacking = true;
            if (isAtackingLastFrame != isAtacking)
            {
                patrol = false;
                if (isWaiting)
                    CancelInvoke("ResetPatrol");

                animator.SetTrigger(attackTrigger);
            }
            if(Time.time > attackTrashhold)
            {
                attackTrashhold = Time.time + attackRate;
                animator.Play(attackTrigger,0,0);

                player.GetComponentInParent<PlayerController>().DealDamage();
            }
        }
        else
        {
            isAtacking = false;

            if (isAtackingLastFrame != isAtacking)
            {

                if (isWaiting && !IsInvoking("ResetPatrol"))
                {
                    Invoke("ResetPatrol", waitTrashhold);
                    if (animator.GetCurrentAnimatorClipInfo(0)[0].clip.name.ToLower() != "idle")
                        animator.SetTrigger(idleTrigger);
                }
                else
                {
                    patrol = true;
                    animator.SetTrigger(walkTrigger);
                }
            }
        }
        
        isAtackingLastFrame = isAtacking;
    }
    void FixedUpdate()
    {
        if (patrol)
            Patrol();
    }
    void Patrol()
    {
        
        if (Vector3.Distance(transform.position, targetPos) < minDistanceToPatrolPoint)
        {
            patrol = false;
            isWaiting = true;
            Invoke("ResetPatrol", waitTrashhold);

            if (animator.GetCurrentAnimatorClipInfo(0)[0].clip.name.ToLower() != "idle")
                animator.SetTrigger(idleTrigger);
            if (targetPos == patrolStart.position)
                targetPos = patrolEnd.position;
            else
                targetPos = patrolStart.position;

        }
        velocity = (transform.position - targetPos).x;
        transform.position = Vector3.MoveTowards(transform.position, targetPos, speed * Time.deltaTime);
    }
    void ResetPatrol()
    {
        patrol = true;
        isWaiting = false;
        animator.SetTrigger(walkTrigger);
        FlipSprite();
    }

    void FlipSprite()
    {
        spriteRenderer.flipX = (velocity < 0) == flipOrientation;
    }
}
