﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[RequireComponent(typeof(AudioSource))]
public class MusicManager : MonoBehaviour
{
    [SerializeField] AudioSource source;
    [SerializeField] AudioClip[] clips;
    Stack<AudioClip> currClips;


    private void Start()
    {
        DontDestroyOnLoad(gameObject);

        CreateStack();
        StartCoroutine(PlayPlayList());

        Application.LoadLevel(Application.loadedLevel + 1);
    }

    void CreateStack()
    {
        currClips = new Stack<AudioClip>(clips.ToList().Shuffle()); 
    }

    IEnumerator PlayPlayList()
    {
        while (true)
        {
            if (currClips.Count == 0)
            {
                    CreateStack();
                yield return null;
            }

            var clip = currClips.Pop();

            source.clip = clip;
            source.Play();
            yield return new WaitForSeconds(clip.length);
            yield return null;

        }
    }
}
