﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Events;

public class Trigger : MonoBehaviour
{
    [SerializeField] UnityEvent myEvent;
    [SerializeField] string playerTag;
    [SerializeField] bool oneUse;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag ==  playerTag)
        {
            IvokeEvent();
            if (oneUse)
                Destroy(gameObject);
        }
        
    }
    public void IvokeEvent()
    {
        myEvent.Invoke();
    }
}
