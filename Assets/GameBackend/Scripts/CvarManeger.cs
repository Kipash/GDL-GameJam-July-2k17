﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Reflection;
using System.Collections.Generic;
using InGameConsole;

public class CvarManeger
{

    static PropertyInfo[] allProperties;
    static Dictionary<string, PropertyInfo> cVars = new Dictionary<string, PropertyInfo>();
    public static void Initialize()
    {
        allProperties = DataStorage.GVars.GetType().GetProperties();
        cVars = allProperties.ToDictionary((x => x.Name), (y => y));
    }

    static string Set(string name, string rawValue)
    {
        var type = cVars[name].PropertyType;

        object val;

        if (type == typeof(string))
        {
            val = rawValue;
        }
        else if (type == typeof(float))
        {
            float f = 0;
            if (float.TryParse(rawValue, out f))
            {
                val = f;
            }
            else
                return "err not a number";
        }
        else
        {
            return "the properity is not known type";
        }


        cVars[name].SetValue(DataStorage.GVars, 0f, null);
        return string.Format("{0} set to {1}", name, rawValue);
    }
    static string Get(string name)
    {
        if (cVars.ContainsKey(name))
        {
            return cVars[name].GetValue(DataStorage.GVars, null).ToString();
        }
        else
        {
            return "err: doesnt contain " + name;
        }
    }
    public static string Use(string[] param)
    {
        if(param.Length == 0) { return "err"; }
        else if(param.Length == 1)
        {
            return Get(param[0]);
        }
        else if(param.Length == 3 && cVars.ContainsKey(param[0]) && param[1] == "=")
        {
            return Set(param[0], param[2]);
        }
        else
        {
            return "to many parameters";
        }
    }
    public static void PrintOutAllCvars()
    {
        foreach (var x in cVars)
        {
            var type = cVars[x.Key].PropertyType;
            string description = "";
            if (type == typeof(string))
            {
                description = "Text";

            }
            else if (type == typeof(float))
            {
                description = "Number";

            }
            else
            {
                description = "Unknown";

            }
            Console.WriteLine(string.Format("{0,-10}| {1} = {2}", description, x.Key, Get(x.Key)));
        }
    }
}
