﻿using UnityEngine;
using System.Collections;

public class GameVaribles
{
    //------- Player -------//
    public float PlayerSpeed { get; set; }
    public string PlayerName { get; set; }
    //------- Player -------//

    //------- Settings -------//
    public float Volume { get; set; }
    //------- Settings -------//


    public GameVaribles()
    {
        PlayerSpeed = 1;
        PlayerName = "Player";
        Volume = 1;
    }
}
