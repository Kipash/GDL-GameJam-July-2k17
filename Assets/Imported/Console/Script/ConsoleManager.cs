﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using InGameConsole;

[Serializable]
public class ConsoleData
{
    public string StartMessage;
    public string LineChar;
    public bool ShowTime;
    public Color TypedCommandColor;
    public Color ErrorCommandColor;
    public Color DefaultTextColor;
    public Color TimeTextColor;
    public Char CommandChar;
    public InputField ConsoleInputField;
    public Text ConsoleLog;
}

public class ConsoleManager : MonoBehaviour
{
    public ConsoleData Data;
    
    
    //Console.Console console;
    public Animator ConsoleAnimotor;
    void Start ()
    {
        InGameConsole.Console.Initialize(Data);
        //console.consoleBase.AddStatic();
        InGameConsole.Console.AddStatic("help", typeof(InGameConsole.Console), "Help");
        InGameConsole.Console.AddStatic("clear", typeof(InGameConsole.Console), "ClearLog");
        InGameConsole.Console.AddStatic("cvar", typeof(CvarManeger), "Use", true);
        InGameConsole.Console.AddStatic("cvars", typeof(CvarManeger), "PrintOutAllCvars");

    }
    bool active = false;
    void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Return))
            InGameConsole.Console.Execute();

        if (Input.GetKeyDown(KeyCode.BackQuote) && (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift)))
        {
            if (active)
            {
                //ConsoleInputField.inte
                Data.ConsoleInputField.DeactivateInputField();
                ConsoleAnimotor.SetTrigger("Pull");
            }
            else
            {
                ConsoleAnimotor.SetTrigger("Push");
                Data.ConsoleInputField.ActivateInputField();
                Data.ConsoleInputField.text = "";
            }
            active = !active;
            //Console.Console.WriteLine("Console is " + active);
        }
    }
}
